'''
Tests for the library assignment - remember to delete all from database before
running
'''
import unittest
from sqlalchemy.exc import OperationalError
from booksmanager import Lender
from models import BASE, ENGINE, PRICE_SETTINGS


# Disable pylint function name quirks
# pylint: disable=C0103
def setUpModule():
    '''
    Setup the modue - Delete all tables if they exist and recreate them
    '''
    try:
        ENGINE.execute('drop table rent')
        ENGINE.execute('drop table books')
    except OperationalError:
        pass
    BASE.metadata.create_all(ENGINE)


def tearDownModule():
    '''
    Empty all tables
    '''
    ENGINE.execute('delete from rent')
    ENGINE.execute('delete from books')


class TestBookMethods(unittest.TestCase):
    '''
    Testing the business logic of the code
    '''
    def test_book_addition(self):
        '''
        Add the book and check whether it is in the library
        '''
        book = Lender()
        book_details = ['Wind in the Willows',
                        123456,
                        'A book about a toad',
                        'fiction']
        book.add_new_book(name=book_details[0],
                          isdn=book_details[1],
                          description=book_details[2],
                          book_type=book_details[3]
                          )
        result = book.get_book(book_details[1])[0]
        self.assertEqual(result.isdn, book_details[1])

    def test_book_rental(self):
        '''
        Rent out the book to a user
        '''
        transaction = Lender()
        renter_details = ["Wilfred", 123456]
        book = transaction.get_book(renter_details[1])[0]
        transaction.rent_book(renter_details[0], renter_details[1])
        renter = transaction.get_renter(renter_details[0],
                                        book.id)[0]
        self.assertEqual(renter.name, renter_details[0])
        self.assertEqual(0, book.in_stock)

    def test_charging_return_book(self):
        '''
        Return the book to the store
        '''
        transaction = Lender()
        renter_details = ["Wilfred", 123456]
        book_details = transaction.get_book(renter_details[1])[0]
        charge = transaction.return_book(renter_details[0], renter_details[1])
        self.assertEqual(float(PRICE_SETTINGS[book_details.book_type.upper()]),
                         charge)

    def test_stetement_generation(self):
        '''
        Generate book store statement
        '''
        transaction = Lender()
        book_details = [
            ['Catcher in the Rye', 234567, 'Another good book', 'novel'],
            ['Things Fall Apart', 345678, 'Good book!', 'regular']]
        for book in book_details:
            transaction.add_new_book(name=book[0],
                                     isdn=book[1],
                                     description=book[2],
                                     book_type=book[3]
                                     )
        renter_details = [["Jimmy", 123456],
                          ["Jimmy", 234567],
                          ["Jimmy", 345678]]
        book_types = []
        for rental in renter_details:
            transaction.rent_book(rental[0], rental[1])
            book_type = transaction.get_book(rental[1])[0].book_type
            book_types.append(book_type)
        statement = transaction.generate_statement("Jimmy")
        self.assertEqual(statement, [['Wind in the Willows',
                                      float(PRICE_SETTINGS[
                                          book_types[0].upper()])],
                                     ['Catcher in the Rye',
                                      float(PRICE_SETTINGS[
                                          book_types[1].upper()])],
                                     ['Things Fall Apart',
                                      float(PRICE_SETTINGS[
                                          book_types[2].upper()])],
                                     ['Total', 6.0]])


SUITE = unittest.TestLoader().loadTestsFromTestCase(TestBookMethods)
unittest.TextTestRunner(verbosity=2).run(SUITE)

# if __name__ == '__main__':
#     unittest.main()
