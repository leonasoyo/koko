# -*- coding: utf-8 -*-
'''
DO functional stuff in here
'''
import inspect
from datetime import datetime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from models import Book, Rent
from models import ENGINE
from models import PRICE_SETTINGS

SESSION = sessionmaker(bind=ENGINE)


class Lender():
    '''
    Manage the lebding and returning of bookss
    '''
    def __init__(self):
        self.session = SESSION()

    def add_new_book(self, name, isdn, description, book_type):
        '''
        Add a new book to the store
        '''
        try:
            new_book = Book(name=name,
                            isdn=isdn,
                            description=description,
                            book_type=book_type)
            self.session.add(new_book)
            self.session.commit()
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise

    def generate_statement(self, name):
        '''
        Generate a receipt for the user to pay
        '''
        statement = []
        try:
            # pylint: disable=C0121
            unpaid = self.session.query(Rent).filter(Rent.paid == False,
                                                     Rent.name == name).all()
            # pylint: enable=C0121
            for rental in unpaid:
                book = self.get_book_by_id(rental.book_id)[0]
                charge = self.return_book(name, book.isdn)
                statement.append([book.name, charge])
            total = sum([rent[1] for rent in statement])
            statement.append(['Total', total])
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise
        return statement

    def get_book(self, isdn):
        '''
        Get a book from the store
        '''
        result = None
        try:
            result = self.session.query(Book).filter(Book.isdn == isdn).all()
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise
        return result

    def get_book_by_id(self, book_id):
        '''
        Retrieve book object by stating its id
        '''
        result = None
        try:
            result = self.session.query(Book).filter(Book.id == book_id).all()
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise
        return result

    def get_renter(self, name, book_id):
        '''
        Get the renter who borrowed the book
        '''
        result = None
        try:
            result = self.session.query(Rent).filter(Rent.name == name,
                                                     Rent.book_id ==
                                                     book_id).all()
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise
        return result

    def rent_book(self, name, isdn):
        '''
        Retrieve a book from the store and give it to a user
        '''
        try:
            book = self.get_book(isdn)[0]
            # Check if book is in stock, if not it is not rentable
            if book.in_stock:
                new_renter = Rent(name=name, book_id=book.id)
                book.in_stock = False
                self.session.add_all([new_renter])
                self.session.commit()
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise

    def return_book(self, name, isdn):
        '''
        Return a book to the store
        '''
        charge = 0
        try:
            book = self.get_book(isdn)[0]
            book.in_stock = True
            renter = self.get_renter(name, book.id)[0]
            renter.date_returned = datetime.now()
            self.session.commit()
            duration = renter.date_returned - renter.creation_date
            price = PRICE_SETTINGS[book.book_type.upper()]
            if duration.days == 0:
                charge = 1 * float(price)
            else:
                charge = duration.days * float(price)
        except SQLAlchemyError:
            print("Error happened at %s" % inspect.stack()[0][3])
            raise
        return charge


if __name__ == '__main__':
    k = Lender()
    # k.add_new_book('Name',1234, 'this ia also a good book')
    # result = k.get_book('123')
    # k.rent_book('Leonard', 1234)
    # print(k.get_renter("Wilfred", 123456))
    #k.return_book("Wilfred", 123456)
    #k.generate_statement('Jimmy')
