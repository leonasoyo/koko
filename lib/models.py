# -*- coding: utf-8 -*-
'''
The models for the application -- do better documentation here
'''

import configparser
import os
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Text, DateTime, Boolean, Enum
from sqlalchemy.ext.declarative import declarative_base

CONFIG_FILE = os.path.join(os.path.dirname(__file__), 'settings.ini')
CONFIG_READER = configparser.ConfigParser()
CONFIG_READER.optionxform = str
CONFIG_READER.read(CONFIG_FILE)
APP_SETTINGS = dict(CONFIG_READER.items('apps'))
PRICE_SETTINGS = dict(CONFIG_READER.items('prices'))
ENGINE = create_engine(APP_SETTINGS['SQL_ALCHEMY_DATABASE_URI'])

BASE = declarative_base()
#Allow pylint to ignore Model/SQLAlchemy quirks
# pylint: disable=R0903
# pylint: disable=C0103
class Foundation():
    '''
    Model upon which others are based
    '''
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, default=datetime.now())
    last_update = Column(DateTime, default=datetime.now(),
                         onupdate=datetime.now())


class Book(BASE, Foundation):
    '''
    Document here
    '''
    __tablename__ = 'books'
    name = Column(String(50))
    isdn = Column(Integer(), unique=True)
    description = Column(Text)
    book_type = Column(Enum("fiction", "novel", "regular"))
    in_stock = Column(Boolean, default=True)

    def __repr__(self):
        return '<Book (name=%s, isdn=%s, description=%s, in_stock=%s)>' % (
            self.name, self.isdn, self.description, self.in_stock)


class Rent(BASE, Foundation):
    '''
    Capture name of the person given the book and the day they returned it
    '''
    __tablename__ = 'rent'
    name = Column(String(50))
    book_id = Column(Integer, ForeignKey('books.id'))
    date_returned = Column(DateTime)
    paid = Column(Boolean, default=False)

    def __repr__(self):
        return ''


if __name__ == "__main__":
    #Run this file to create the databse tables if they do not exist yet
    BASE.metadata.create_all(ENGINE)
