Hi Leonard,
Below is a programming exercise that we expect you to solve and come up with a solution.
We look out less for completeness of solution in this round but more for the approach and the design you come up with. Look out for necessary domain models, right behaviour, code designing fundamentals and Clean code when you develop your solution.
Do add to write the tests for your solution.
It is broken into 2 parts:
Part 1 - Coding exercise (functional requirements)
Part 2 - Non functional requirements

Part 1: Coding exercise (functional requirements)
Language - Any of your choice, but we would prefer a mainstream language like Python, Ruby, Java or Javascript.

Problem Statement
It is a program to calculate and print a statement of a customer's charges at a book rental store. 
It is divided into 3 stories. Pick each story in sequence, solve it, test it, and then pick up next. Next story should be applied in same solution and your design should start evolving.

(Note -  You need to create just one solution only which would satisfy all 3 stories)

Story 1:
Customers can rent the books from the store. The rent changes will be calculated on the basis of number of books rented and durations for each book it was rented. Per day rental charge is Rs 1.
Story 2:
There are three kinds of books: regular, fiction, and novels. For Regular books renting per day charge is Rs. 1.5. For fiction book renting per day charge is Rs. 3. For novels the per day charge is Rs. 1.5.
Story 3:
The store decided to alter the calculations for Regular books and novels. Now for Regular books for first 2 days charges will be Rs 1 per day and 1.5 Rs there after. Minimum changes will be considered as Rs 2 if days rented is less than 2 days. Similarly for Novel minimum charges are introduced as 4.5 Rs if days rented is less than 3 days.

We'd like to see how your solution evolves with every new story. So, please implement the stories in the given order, with at least one commit per story (of course, you may create as many intermediate commits as you like). You may share your solution either as a zip file (including the commits) or through github.


Part 2: Non Functional requirements:
1. If we were to expose an API for the above functionality for Mobile apps, how many concurrent requests can the application support? 
2. What the ways we can increase/modify the requests per second rate if we need higher processing rate?
3. Please describe the deployment process along with branching strategy for this code to be deployed to various environments.

Kindly send your response by Thursday(13th Sep 2018) evening.
Please let me know if you have any queries.

Thanks
